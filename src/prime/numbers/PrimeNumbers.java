package prime.numbers;

import java.util.HashMap;


public class PrimeNumbers{

    private int size = 1_000_00;
    private HashMap<Integer, Integer> numbers = new HashMap(){
        {
            /*
             * Puts all numbers from 0 to "size" in a hashmap so if
             *  we need to remove some numbers to make it faster, we
             *  can easily do that
             */
            for(int i = 0; i < size; i++){
                put(i, i);
            }
        }

    };


    public static void main(String[] args){
        new PrimeNumbers();
    }


    public PrimeNumbers(){
        long startTime = System.nanoTime();
        for(int i = 0; i < size; i++){
            ///if(fermatsLittleTheorem(i)){
            if(isPrime(i)){
                System.out.println(i + " is prime");
            }
            //remove(i);
        }
        long endTime = System.nanoTime();
        //Print time out in seconds
        System.out.println("Time taken: " + (endTime - startTime) / 1_000_000_000.0 + "s");
    }


    /**
     * Brute force calculating if its prime, loops through every number
     * from 2 until the square of the checking number is greater than
     * the number to be checked
     *
     * @param p the number to check
     *
     * @return true if the number is prime
     */
    private boolean isPrime(int p){
        for(int i = 2; i * i <= p; i++){
            if(p % i == 0){
                return false;
            }
        }
        return true;
    }


    /**
     * Tries to remove all multiples of every checked number to make it run faster,
     * and while the main loop is actually improved, this loop more than cancels out
     * the speed increase
     *
     * @param n the number to remove from the hashmap
     */
    private void remove(int n){
        if(n <= 2){
            return;
        }
        //remove the checked number and all multiples of it to speed up
        for(int i = n; i < numbers.size() - 1; i += n){
            numbers.remove(i);
        }
    }


    /**
     * Major overflowing issues, stops being reliable at 13
     *
     * @param p
     *
     * @return
     */
    private boolean fermatsLittleTheorem(int p){
        for(int a = 2; a < p; a++){
            if((power(a, p) - a) % p != 0){
                return false;
            }
        }
        return true;
    }


    public double power(int x, int n){
        double z = x;
        for(int i = 1; i < n; i++){
            z *= x;
        }
        return z;
    }

}
